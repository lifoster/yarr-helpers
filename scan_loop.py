from helper import *
import numpy as np
import file_paths as fp
import scan_loop_helper as sl_h
import loop_inputs as li
import pandas as pd

occ = []
reg = "DiffTh1"

loop_vals = li.loop_vals

def run_scan_loop():
    for val in loop_vals:
        # set_global_register("DiffTh1M",int(val))
        # set_global_register("DiffTh1L",int(val))
        # set_global_register("DiffTh1R",int(val))
        set_scan_parameter("global","max",int(val))
        set_scan_parameter("global","min",int(val))
        run_globalthreshold_tune()
        occ.append(sl_h.get_occupancy(fp.last_scan))
    data = {'x_vals': loop_vals, 'Occupancy': occ}
    df = pd.DataFrame(data)
    return(df)

# def run_scan_loop():
#     for val in loop_vals:
#         set_global_register("DiffTh1M",int(val))
#         set_global_register("DiffTh1L",int(val))
#         set_global_register("DiffTh1R",int(val))
#         run_analog_scan()
#         reset_chip()