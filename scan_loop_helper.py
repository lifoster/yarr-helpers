import numpy as np
import json
import pandas as pd

# def get_analog_occupancy(file):
#     with open(file+"JohnDoe_0_OccupancyMap.json", 'r') as f:
#         data = json.load(f)
        
#         total_sum = 0
#         total_count = 0
        
#         for sub_array in data['Data']:
#             total_sum += sum(sub_array)
#             total_count += len(sub_array)
        
#         average = total_sum / total_count
#         return average

def get_occupancy(json_path, bin_nums = np.arange(51)):
    with open(json_path + "JohnDoe_0_OccupancyDist-0.json", 'r') as f:
        data = json.load(f)

    overflow = data['Overflow']
    underflow = data['Underflow']

    occ_bin = np.linspace(data['x']['Low'], data['x']['High'], data['x']['Bins'])
    occ_bin = np.insert(occ_bin, 0, 0.)
    occ_bin = np.append(occ_bin, 50.)

    bin_content = np.array(data['Data'])
    bin_content = np.insert(bin_content, 0, underflow)
    bin_content = np.append(bin_content, overflow)

    mask = np.zeros_like(bin_content, dtype = bool)
    mask[bin_nums] = True
    bin_content[~mask] = 0

    bin_content /= bin_content.sum()

    average = 2*np.dot(occ_bin, bin_content)
    return average

def get_bathtub(json_path):
    with open(json_path + "JohnDoe_0_OccupancyDist-0.json", 'r') as f:
        data = json.load(f)

    overflow = data['Overflow']
    underflow = data['Underflow']

    occ_bin = np.linspace(data['x']['Low'], data['x']['High'], data['x']['Bins'])
    occ_bin = np.insert(occ_bin, 0, 0.)
    occ_bin = np.append(occ_bin, 50.)

    bin_content = np.array(data['Data'])
    bin_content = np.insert(bin_content, 0, underflow)
    bin_content = np.append(bin_content, overflow)

    df = pd.DataFrame({'x_vals': occ_bin, 'count': bin_content})
    return df

def get_bathtub_bin(json_path, bin):
    with open(json_path + "JohnDoe_0_OccupancyDist-0.json", 'r') as f:
        data = json.load(f)

    n_pixels = 400*384

    overflow = data['Overflow']
    underflow = data['Underflow']
    bin_content = data['Data']
    if (bin == 0):
        return underflow
    if (bin == 50):
        return overflow
    return bin_content[bin-1]

def get_DiffTh1(file):
    with open(file+"JohnDoe_0_OccupancyMap.json", 'r') as f:
        config = json.load(f)
    return(config["ITKPIXV2"]["GlobalConfig"].get("DiffTh1M", None))