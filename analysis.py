import numpy as np
import file_paths as fp
import scan_loop_helper as slh
import loop_inputs as li
import loop as l
import matplotlib.pyplot as plt
import pandas as pd
import plot
import imageio
from helper import *

#TODO high bins and low bins separate inputs?
# df = l.bathtub_analysis([0,50], li.folders)
# plot.run_plot(df,"DiffTh1","Number of Pixels")

# l.analog_gif(li.folders)

# min_bin = 0
# max_bin = 50
# n=1
# bins = np.arange(min_bin+n,max_bin-n+1)
# low_bins = np.arange(min_bin, min_bin + n)
# high_bins = np.arange(max_bin - n, max_bin + 1)
# bins = np.concatenate((low_bins, high_bins))
# df = l.occupancy_analysis(bins, li.folders)
# plot.run_plot(df,"DiffTh1","Noise Contribution to Bathtub in Old Chip")

# bathtubs = []
# diffth = []
# for folder in li.folders:
#     bathtubs.append(slh.get_bathtub(folder)[1:-1])
#     diffth.append(get_global_register("DiffTh1M",folder+"itkpixv2_test.json.after"))
# plot.run_gif(bathtubs, diffth, "DiffTh1")

avg = []
integral = []
diffth = []
for folder in li.folders:
    df = slh.get_bathtub(folder)[1:-1]
    avg.append(np.dot(df['x_vals'],df.iloc[:,1])/np.sum(df.iloc[:,1]))
    integral.append(np.sum(df.iloc[:,1]))
    diffth.append(get_global_register("DiffTh1M",folder+"itkpixv2_test.json.after"))
    
integral = np.array(integral)
avg = np.array(avg)
norm = 1./np.sum(avg)
integral /= np.sum(integral)
avg *= norm

df_new = pd.DataFrame({'x_vals':diffth, 'Average Occupancy':avg, 'Integral':integral})
plot.run_plot(df_new,"DiffTh1","Arbitrary Units",title="Comparing Tuning Metrics",hline=(25.*norm))