from helper import *
import scan_loop as sl
import plot as pl
import pandas as pd
import time

# init()
# clean_run_dir()
# reset_tdacs()

# to_plot = sl.run_scan_loop()
# pl.run_plot(to_plot,'DiffTh1','Mean Occupancy')
# reset_chip()
# df = sl.run_scan_loop()
# pl.run_plot(df,'DiffTh1','Mean Occupancy')

# init()
# clean_run_dir()
# reset_chip()
# df = sl.run_scan_loop()
# pl.run_plot(df, 'DiffTh1', 'Mean Occupancy', 'New Chip')

# init()
# clean_run_dir()
# reset_chip()
# run_globalthreshold_tune(1000)
# run_threshold_scan()
# run_plotting_tools()

# init()
# clean_run_dir()
# reset_chip()

# start_g = time.time()
# run_globalthreshold_tune(1000)
# end_g = time.time()

# run_threshold_scan()

# start_p = time.time()
# run_pixelthreshold_tune(1000)
# end_p = time.time()
# run_analog_scan(2000)
# run_threshold_scan()

# print("Global Threshold Tuning Duration:",end_g-start_g)
# print("Pixel Threshold Tuning Duration:",end_p-start_p)
# print("Total Tuning Time:",end_g-start_g + end_p-start_p)


# old
# init()
# clean_run_dir()
# reset_chip()

# sg1 = time.time()
# run_globalthreshold_tune(2000)
# eg1 = time.time()
# run_threshold_scan()
# sp1 = time.time()
# run_pixelthreshold_tune(2000)
# ep1 = time.time()
# run_threshold_scan()

# sg2 = time.time()
# run_globalthreshold_retune(1500)
# eg2 = time.time()
# run_threshold_scan()
# sp2 = time.time()
# run_pixelthreshold_retune(1500)
# ep2 = time.time()
# run_threshold_scan()

# sg3 = time.time()
# run_globalthreshold_retune(1000)
# eg3 = time.time()
# run_threshold_scan()
# sp3 = time.time()
# run_pixelthreshold_retune(1000)
# ep3 = time.time()
# run_threshold_scan()

# run_analog_scan(3000)

# print("Total Global Threshold Tuning Duration:",(eg1-sg1 + eg2-sg2 + eg3-sg3))
# print("Pixel Threshold Tuning Duration:",(ep1-sp1 + ep2-sp2 + ep3-sp3))
# print("Total Tuning Time:", eg1-sg1 + eg2-sg2 + eg3-sg3 + ep1-sp1 + ep2-sp2 + ep3-sp3)

# global threshold range
# init()
# clean_run_dir()
# reset_chip()
# thr = np.arange(0, 4001, 100)
# for th in thr:
#     run_globalthreshold_tune(th)
#     run_threshold_scan()

#fkn pixel tuning innit
init()
clean_run_dir()
reset_chip()
# run_globalthreshold_tune(2000)
# run_threshold_scan()
run_pixelthreshold_tune(2000)
run_threshold_scan()