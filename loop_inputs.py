import numpy as np
import file_paths as fp

loop_vals = np.arange(0,301,2)
# loop_vals = np.arange(100,301,25)

start = 891
end = 1041
# for slide 14
folder_name_base = "_std_tune_globalthreshold/"
run_numbers = np.arange(start, end + 1)
folders = [fp.old_scans+str(i).zfill(6)+folder_name_base for i in run_numbers]

# # analog gifs
# start = 1067
# end = 1075

# folder_name_base = "_std_analogscan/"
# run_numbers = np.arange(start, end + 1)
# folders = [fp.output_directory+str(i).zfill(6)+folder_name_base for i in run_numbers]

# # no injection
# start = 1091
# end = 1241
# folder_name_base = "_std_tune_globalthreshold/"
# run_numbers = np.arange(start, end + 1)
# folders = [fp.old_scans+str(i).zfill(6)+folder_name_base for i in run_numbers]

# for test bin selection plots
# start = 891
# end = 1041
# folder_name_base = "_std_tune_globalthreshold/"
# run_numbers = np.arange(start, end + 1)
# folders = [fp.old_scans+str(i).zfill(6)+folder_name_base for i in run_numbers]