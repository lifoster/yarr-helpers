import sys
import os
import numpy as np
import argparse
import json
import file_paths as fp
import shutil

def get_scan_number():
    with open(fp.last_scan + 'scanLog.json', 'r') as scan_log:
        runNumber = json.load(scan_log)['runNumber']
    return runNumber

def run_plotting_tools(scan):
    runNumber = get_scan_number()
    os.system("./bin/plotFromDir -i " + fp.output_directory + "00" + str(runNumber) + "_std_"+scan+"/ -p png -P")

def run_digital_scan():
    digital_scan = "./bin/scanConsole -r configs/controller/specCfg-rd53b-4x4.json -c configs/connectivity/example_itkpixv2_setup.json -s configs/scans/itkpixv2/std_digitalscan.json -p -o " + fp.output_directory
    os.system(digital_scan)
    run_plotting_tools("digitalscan")
    return get_scan_number()

def run_globalthreshold_tune(t = 1000):
    globalthreshold_tune = "./bin/scanConsole -r configs/controller/specCfg-rd53b-4x4.json -c configs/connectivity/example_itkpixv2_setup.json -s configs/scans/itkpixv2/std_tune_globalthreshold.json -t " + str(t) + " -o " + fp.output_directory
    os.system(globalthreshold_tune)
    run_plotting_tools("tune_globalthreshold")
    return get_scan_number()

def run_globalthreshold_retune(t = 1000):
    globalthreshold_tune = "./bin/scanConsole -r configs/controller/specCfg-rd53b-4x4.json -c configs/connectivity/example_itkpixv2_setup.json -s configs/scans/itkpixv2/std_retune_globalthreshold.json -t " + str(t) + " -p -o " + fp.output_directory
    os.system(globalthreshold_tune)
    return get_scan_number()

def run_pixelthreshold_tune(t = 1000):
    pixelthreshold_tune = "./bin/scanConsole -r configs/controller/specCfg-rd53b-4x4.json -c configs/connectivity/example_itkpixv2_setup.json -s configs/scans/itkpixv2/std_tune_pixelthreshold.json -t " + str(t) + " -o " + fp.output_directory
    os.system(pixelthreshold_tune)
    run_plotting_tools("tune_pixelthreshold")
    return get_scan_number()

def run_pixelthreshold_tune_old(t = 1000):
    pixelthreshold_tune_old = "./bin/scanConsole -r configs/controller/specCfg-rd53b-4x4.json -c configs/connectivity/example_itkpixv2_setup.json -s configs/scans/itkpixv2/std_tune_pixelthreshold_old.json -t " + str(t) + " -p -o " + fp.output_directory
    os.system(pixelthreshold_tune_old)
    return get_scan_number()

def run_pixelthreshold_retune(t = 1000):
    pixelthreshold_retune = "./bin/scanConsole -r configs/controller/specCfg-rd53b-4x4.json -c configs/connectivity/example_itkpixv2_setup.json -s configs/scans/itkpixv2/std_retune_pixelthreshold.json -t " + str(t) + " -p -o " + fp.output_directory
    os.system(pixelthreshold_retune)
    return get_scan_number()

def run_threshold_scan():
    threshold_scan = "./bin/scanConsole -r configs/controller/specCfg-rd53b-4x4.json -c configs/connectivity/example_itkpixv2_setup.json -s configs/scans/itkpixv2/std_thresholdscan.json -o " + fp.output_directory
    os.system(threshold_scan)
    run_plotting_tools("thresholdscan")
    return get_scan_number()

def run_analog_scan(t = 10000):
    analog_scan = "./bin/scanConsole -r configs/controller/specCfg-rd53b-4x4.json -c configs/connectivity/example_itkpixv2_setup.json -s configs/scans/itkpixv2/std_analogscan.json -t " + str(t) + " -m 1 -p -o " + fp.output_directory
    os.system(analog_scan)
    run_plotting_tools("analogscan")
    return get_scan_number()

def reset_failing_tdacs(t_num, a_num):
    n_dead = 0
    with open(fp.output_directory + "000" + str(t_num) + "_std_thresholdscan/itkpixv2_test.json.after", 'r') as th_json:
        chip_config = json.load(th_json)
    pixelConfig = chip_config["ITKPIXV2"]["PixelConfig"]

    with open(fp.output_directory + "000" + str(a_num) + "_std_analogscan/JohnDoe_0_EnMask.json", 'r') as an_json:
        en_mask = json.load(an_json)["Data"]

    for config in pixelConfig:
        tdacs = config["TDAC"]
        col = config["Col"]
        enabled = config["Enable"]
        for row in range(len(tdacs)):
            if en_mask[col][row] == 0.0:
                # print(f"TDAC value for Pixel ({col}, {row}) before update: {tdacs[row]}") #Writes the TDAC value of the masked pixel
                tdacs[row] = 15
                n_dead += 1

    with open(output_config, 'w') as config_out:
        json.dump(chip_config, config_out)
        # print(f"Updated TDAC values have been written to {output_config}")
    return n_dead

def set_global_register(register, value):
    with open(fp.chip_config) as config_json:
        config = json.load(config_json)
    config["ITKPIXV2"]["GlobalConfig"][register] = value
    
    with open(fp.chip_config, 'w') as config_out:
        json.dump(config, config_out)

def get_global_register(register, chip_config = fp.chip_config):
    with open(chip_config) as config_json:
        config = json.load(config_json)
    return config["ITKPIXV2"]["GlobalConfig"][register]

def reset_tdacs(rst = 0):
    with open(fp.chip_config) as config_json:
        chip_config = json.load(config_json)
    pixelConfig = chip_config["ITKPIXV2"]["PixelConfig"]

    for config in pixelConfig:
        tdacs = config["TDAC"]
        config["TDAC"] = np.full_like(tdacs, rst).tolist()

    with open(fp.output_config, 'w') as config_out:
        json.dump(chip_config, config_out)

def reset_mask():
    with open(fp.chip_config) as config_json:
        chip_config = json.load(config_json)
    pixelConfig = chip_config["ITKPIXV2"]["PixelConfig"]

    for config in pixelConfig:
        enable = config["Enable"]
        config["Enable"] = np.full_like(enable, 1).tolist()

    with open(fp.output_config, 'w') as config_out:
        json.dump(chip_config, config_out)

def reset_chip():
    reset_tdacs()
    reset_mask()

def set_scan_parameter(scan, parameter, value):
    if(scan == "global"):
        config_link = "/home/liam/YARR/configs/scans/itkpixv2/std_tune_globalthreshold.json"
    else:
        print("set_scan_parameter() only for global threshold tunings. Go annoy Liam about it")
    with open(config_link) as config_json:
        scan_config = json.load(config_json)
    scan_config["scan"]["loops"][0]["config"][parameter] = value

    with open(config_link, 'w') as config_out:
        json.dump(scan_config, config_out)

def clean_run_dir():
    source_contents = os.listdir(fp.output_directory)

    for item in source_contents:
        source_item_path = os.path.join(fp.output_directory, item)
        destination_item_path = os.path.join(fp.old_scans, item)

        shutil.move(source_item_path, destination_item_path)

def init():
    parser = argparse.ArgumentParser(description='New Tune Input Parameters')
    # Add argument for integer parameter
    parser.add_argument('-t', '--threshold_target', help='Threshold Target', default = 1000)
    parser.add_argument('-ts', '--run_threshold', type=bool, help='Whether To Run Threshold Scan after Global Threshold Tuning', default = True)

    args = parser.parse_args()

    t = args.threshold_target
    ts = args.run_threshold

    if not os.path.exists(fp.output_directory):
        os.makedirs(fp.output_directory)
