import matplotlib.pyplot as plt
import numpy as np

noise = np.array([265.0, 165.0, 106.0, 75.0, 56.0, 64.0, 46.0, 32.0, 33.0, 29.0, 30.0, 24.0, 31.0, 24.0, 17.0, 19.0, 18.0, 20.0, 21.0, 17.0, 15.0, 19.0, 20.0, 16.0,
         23.0, 8.0, 11.0, 9.0, 17.0, 11.0, 11.0, 9.0, 13.0, 11.0, 6.0, 17.0, 15.0, 6.0, 13.0, 11.0, 13.0, 13.0, 17.0, 11.0, 6.0, 7.0, 13.0, 8.0, 12.0])
bathtub_120 = np.array([3180.0, 1962.0, 1364.0, 1128.0, 931.0, 832.0, 771.0, 689.0, 611.0, 591.0, 558.0, 550.0, 497.0, 456.0, 489.0, 496.0, 436.0, 421.0, 394.0, 
415.0, 432.0, 431.0, 441.0, 409.0, 373.0, 415.0, 401.0, 416.0, 398.0, 428.0, 416.0, 430.0, 459.0, 492.0, 482.0, 456.0, 499.0, 516.0, 517.0, 590.0, 636.0, 575.0, 
720.0, 813.0, 896.0, 1058.0, 1263.0, 1707.0, 2941.0])

new_noise = np.array([279.0, 152.0, 111.0, 87.0, 80.0, 65.0, 61.0, 57.0, 52.0, 36.0, 26.0, 47.0, 50.0, 31.0, 43.0, 43.0, 35.0, 30.0, 37.0, 41.0, 35.0, 36.0,
 30.0, 30.0, 38.0, 28.0, 25.0, 27.0, 29.0, 29.0, 41.0, 26.0, 29.0, 30.0, 36.0, 29.0, 33.0, 32.0, 47.0, 28.0, 27.0, 36.0, 32.0, 34.0, 40.0, 33.0, 27.0, 32.0, 24.0])


bins = np.linspace(1,49,49)

plt.bar(bins, noise,width=1,align='center',label='Old Noise',alpha=0.5)
plt.bar(bins, new_noise,width=1,align='center',label='New Noise',alpha=0.5)
plt.legend()
plt.savefig("blahbathtub.png")
plt.close()

# bathtub_120 = bathtub_120 - noise

# avg = np.average(np.dot(bins,bathtub_120)/np.sum(bathtub_120))
# target = 25

# plt.bar(bins, bathtub_120,width=1,align='center')
# plt.axvline(x=avg,color='orange',linestyle='--')
# plt.axvline(x=target,color='green',linestyle='--')
# plt.title("Bathtub at Threshold with Old Chip, with Noise Subtracted")
# plt.ylabel("Number of Pixels per Injection")
# plt.xlabel("Injections")
# plt.savefig("blahbathtub.png")
# plt.close()