import numpy as np
import matplotlib.pyplot as plt
import imageio

global_threshold_start = 0.  # e
injection_size = 1000. # e, injection size
thr_sigma = 250.  # e
noise_disp = 10.  # e, taking as sigma, something wrong here
n_rows = 384
n_cols = 400

bathtub_noise = 10*np.array([265.0, 165.0, 106.0, 75.0, 56.0, 64.0, 46.0, 32.0, 33.0, 29.0, 30.0, 24.0, 31.0, 24.0, 17.0, 19.0, 18.0, 20.0, 21.0, 17.0, 15.0, 19.0, 20.0, 16.0, 23.0, 8.0, 11.0, 9.0, 17.0, 11.0, 11.0, 9.0, 13.0, 11.0, 6.0, 17.0, 15.0, 6.0, 13.0, 11.0, 13.0, 13.0, 17.0, 11.0, 6.0, 7.0, 13.0, 8.0, 12.0],dtype=int)
print(bathtub_noise.size)

num_pixels = n_rows * n_cols
pixel_thr = np.zeros(num_pixels, dtype=float)
pixel_noise = np.zeros(num_pixels, dtype=float)

pixel_thr = np.random.default_rng().normal(global_threshold_start, thr_sigma, num_pixels)
pixel_noise = np.random.default_rng().normal(50., noise_disp, num_pixels)
plt.hist(pixel_noise)
plt.savefig("blah4.png")
plt.close()

scan_pts = np.arange(800,1200,25)
injections = np.arange(0,50)
occ_values = []
frames = []
avg = []
integral = []

# g_thr is new global threshold set by a register in the chip
# global threshold starts at zero in this loop, goes to 3000
for g_thr in scan_pts:
    pixel_occ = np.zeros_like(pixel_thr, dtype=float)
    for inj in injections:
        thr = pixel_thr + g_thr
        nse = np.random.default_rng().normal(0., pixel_noise, num_pixels)
        mask = injection_size + nse > thr
        pixel_occ[mask] += 1.

    pixel_occ /= float(injections[-1]+1)
    pixel_occ[pixel_occ == 1.] = 0.0
    occ_values.append(np.sum(pixel_occ)/num_pixels)

    bin_edges = np.linspace(0,1,52)
    bathtub = np.histogram(pixel_occ,bins=bin_edges)[0]
    print(bathtub[1:-1].size)
    bathtub[1:-1] += bathtub_noise
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2
    loc_avg = np.average(np.dot(bin_centers[1:-1],(bathtub[1:-1])/(np.sum(bathtub[1:-1]))))
    avg.append(loc_avg)
    integral.append((np.sum(bathtub[1:-1]))/float(num_pixels))
    plt.bar(bin_centers[1:-1],bathtub[1:-1],width=bin_edges[1],label=("threshold = " + str(g_thr)+"e"),align='center')
    plt.axvline(x=loc_avg,color='orange',linestyle='--')
    plt.axvline(x=0.5,color='green',linestyle='--')
    plt.legend(loc="upper right")

    if (g_thr == 1000):
        plt.savefig("blah2.png")

    fig = plt.gcf()
    fig.canvas.draw()
    image = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
    image = image.reshape(fig.canvas.get_width_height()[::-1] + (3,))
    plt.close()
    frames.append(image)

imageio.mimsave('blah.gif', frames, duration=1.0)

plt.plot(scan_pts, occ_values)
# plt.plot(scan_pts, avg, color='green')
# plt.plot(scan_pts, integral, color='red')
plt.axvline(x = injection_size, linestyle='--',color='orange')
plt.xlabel("Global Threshold")
plt.ylabel("Occupancy")
plt.savefig("blah.png")