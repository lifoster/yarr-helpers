import numpy as np
import file_paths as fp

start = 891
end = 1041
folder_name_base = "_std_tune_globalthreshold/"
run_numbers = np.arange(start, end + 1)
folders = [fp.output_directory+str(i).zfill(6)+folder_name_base for i in run_numbers]