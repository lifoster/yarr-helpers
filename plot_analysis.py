import os
import imageio
import file_paths as fp
import numpy as np

def create_gif(folder_names, output_path):
    images = []
    for folder_name in folder_names:
        pdf_path = os.path.join(folder_name, 'plots.pdf')
        if os.path.exists(pdf_path):
            image_path = os.path.join(folder_name, 'plots.png')
            os.system(f'convert -density 150 "{pdf_path}" "{image_path}"')
            images.append(imageio.imread(image_path))

    imageio.mimsave(output_path, images, duration=1)

start = 70
end = 79

run_numbers = np.arange(start, end + 1)
folders = [fp.plot_dir + "plot" + str(i) for i in run_numbers]
output_path = folders[-1] + '/output.gif'
create_gif(folders, output_path)