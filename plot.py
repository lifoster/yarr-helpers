import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

import numpy as np
import file_paths as fp
import json
import os
import imageio

def run_plot(df, xaxis, yaxis, title, marker = '.', style = "default", target_lines = True, legend = True, print = True, set_ylim = -9999, avg_line = False, hline = -9999):
    # Define default colors
    num_lines = (len(df.columns)) // 2
    red_colors = plt.cm.Reds(np.linspace(0.5, 1, num_lines))
    blue_colors = plt.cm.Blues(np.linspace(0.5, 1, num_lines))
    colors = np.concatenate((red_colors[::-1], blue_colors))


    # Plot
    plt.figure(figsize=(8, 6))
    df.plot(x='x_vals', marker=marker, xlabel=xaxis, ylabel=yaxis, color=colors, ds=style)
    if (target_lines):
        plt.axvline(x=120,linestyle='--', color='orange')
        if (hline != -9999):
            plt.axhline(y=hline,linestyle='--',color='violet')
    if(avg_line):
        avg = np.dot(df['x_vals'],df.iloc[:,1])/np.sum(df.iloc[:,1])
        plt.axvline(x=avg,linestyle='--',color='red')
        plt.axhline(y=np.sum(df.iloc[:,1])/40,linestyle='--',color='green')
    if (legend):
        plt.legend(loc = 'upper right')
    else:
        plt.legend([])
    if (set_ylim != -9999):
        plt.ylim(0, set_ylim)

    # Add labels and title
    plt.xlabel(xaxis)
    plt.ylabel(yaxis)
    plt.title(title)

    if (print):
        with open(fp.plot_info) as plt_json:
            plt_info = json.load(plt_json)
        plt_num = plt_info["plot_number"]

        plt_info["plot_number"] = plt_num + 1
        plt_info["x_vals"] = df['x_vals'].to_dict()
        plt_info["y_vals"] = df.drop(columns ='x_vals').to_dict()

        with open(fp.plot_info, 'w') as plt_info_out:
            json.dump(plt_info, plt_info_out, indent=4)

        plot_dir = fp.plot_dir+"plot"+str(plt_num)+"/"
        if not os.path.exists(plot_dir):
            os.makedirs(plot_dir)
        pdf_pages = PdfPages(plot_dir+'plots.pdf')
        
        with open(plot_dir+"plot_info.json", 'w') as json_out:
            json.dump(plt_info, json_out, indent = 4)

        pdf_pages.savefig()
        pdf_pages.close()
        plt.close()

    else:
        fig = plt.gcf()
        fig.canvas.draw()
        image = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
        image = image.reshape(fig.canvas.get_width_height()[::-1] + (3,))
        plt.close()
        return image

def run_gif(df_array, index, index_name):
    with open(fp.plot_info) as plt_json:
        plt_info = json.load(plt_json)
    plt_num = plt_info["plot_number"]

    plt_info["plot_number"] = plt_num + 1
    plt_info["x_vals"] = [0]
    plt_info["y_vals"] = [0]

    with open(fp.plot_info, 'w') as plt_info_out:
        json.dump(plt_info, plt_info_out, indent=4)

    plot_dir = fp.plot_dir+"plot"+str(plt_num)+"/"
    if not os.path.exists(plot_dir):
        os.makedirs(plot_dir)
    
    with open(plot_dir+"plot_info.json", 'w') as json_out:
        json.dump(plt_info, json_out, indent = 4)

    images = []
    for i, df in enumerate(df_array):
        image = run_plot(df,"Hits", "Pixels", index_name + " = " + str(index[i]), marker = "None", style = "steps-mid", target_lines = False, legend = False, print = False, set_ylim = 3500, avg_line = True)
        images.append(image)
    imageio.mimsave(plot_dir+'plot.gif', images[::-1], duration=0.25)