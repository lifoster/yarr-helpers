import numpy as np
from helper import *
import scan_loop_helper as slh
import loop_inputs as li
import pandas as pd
import os
import imageio
from PIL import Image, ImageDraw, ImageFont

#TODO REWRITE THIS USING SOMETHING LIKE data = {'x_vals': loop_vals, 'bin1': bin1Data}; df= pd.DataFrame(data)
def bathtub_analysis(bins, folders):
    df_labels = ["bin" + str(num) for num in bins]
    df_labels = ["x_vals"] + df_labels

    vals = np.zeros((len(bins)+1,len(folders)))
    for i, xval in enumerate(li.loop_vals):
        vals[0][i] = xval

    for j, folder in enumerate(folders):
        for i, bin_num in enumerate(bins):
            vals[i+1][j] = slh.get_bathtub_bin(folder, bin_num)

    df = pd.DataFrame(vals, index = df_labels)
    return df.transpose()


def occupancy_analysis(bins, folders):
    df_labels = ["Bins " + str(bins[0]) + " - " + str(bins[-1])]
    df_labels = ["x_vals"] + df_labels
    occ_data = []
    for folder in folders:
        occ_data.append(slh.get_occupancy(folder, bins))
    data = {"x_vals": li.loop_vals, "Occupancy": occ_data}
    return pd.DataFrame(data)


def analog_gif(folders):
    images = []
    for folder in folders:
        image = Image.open(folder + "JohnDoe_0_EnMask.png")
        image = image.convert("RGB")
        number = get_global_register("DiffTh1M", folder + "itkpixv2_test.json.after")
        font = ImageFont.truetype(fp.font, 20)
        draw = ImageDraw.Draw(image)
        draw.text((image.width - 190, image.height - 30), f'DiffTh1: {number}', font=font, fill=(0, 0, 0))
        images.append(image)
    images.reverse()
    imageio.mimsave("output.gif", images, duration=0.5)