import json
import numpy as np
import matplotlib.pyplot as plt

# def get_en_mask(file):
#     with open(file, 'r') as f:
#         data = json.load(f)
#     return np.concatenate(np.array(data['Data']))

# def get_tdacs(file):
#     with open(file) as config_json:
#         chip_config = json.load(config_json)
#     pixelConfig = chip_config["ITKPIXV2"]["PixelConfig"]
#     tdacs = np.array([])
#     for config in pixelConfig:
#         tdac_row = config["TDAC"]
#         tdacs = np.concatenate((tdacs,tdac_row))
#     return tdacs

# en_mask = get_en_mask("/home/liam/YARR/data/run/001692_std_analogscan/JohnDoe_0_EnMask.json")
# tdacs = get_tdacs("/home/liam/YARR/configs/itkpixv2_test.json")

# mask = en_mask == 0

# bins = np.arange(-15,16)
# plt.hist(tdacs[mask],bins=bins)
# plt.xlabel("TDAC of Failing Pixel")
# plt.savefig("blahHist.png")

# thr_target = np.arange(1000, 2001, 100)
# thr = [1900,1850,1700,1650,1550,1450,1350,1200,1150,1100,950]
# thr = thr[::-1]

thr_target = np.arange(0, 2501, 100)
thr = [1897.9,450,450,450,550,600,550,700,750,850,987,1075,1133,1176,1437,1487,1548,1646,1739,1849,1904,2000,2125,2225,2350,2375]
errors = 100
# thr = thr[::-1]
print (thr_target[7])
print (thr[7])

plt.errorbar(thr_target, thr, yerr=errors,fmt='o')
plt.plot(thr_target, thr_target, color = 'orange', linestyle = '--')
plt.xlabel("Target Global Threshold [e]")
plt.ylabel("Measured Global Threshold [e]")
plt.savefig("blahTarget.png")